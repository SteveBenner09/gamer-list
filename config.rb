# Compass configuration for 'The Gamer List' website

project_path    = 'source'
http_path       = '/'
css_dir         = 'css'
sass_dir        = 'sass'
images_dir      = 'img'
javascripts_dir = 'js'
fonts_dir       = 'fonts'

output_style    = :compressed
line_comments   = false