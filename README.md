# PUC Gamer List
This was a side-project of mine during CS studies at PUC, inspired by the desire to expand my programming skills. I wanted to develop web apps, so I designed and created this site from scratch in order to acquire the necessary capabilities. I presented it with the humble concept of fostering social connection among my school's gaming community, but primarily it was a personal learning experience of immense value.

- All [source code](source/) and images are original, except for assets within `vendor` subdirectories.

### Demonstrated technical feats
- Dynamic HTML generation, micro-templating
- Custom data parsing (XML, JSON)
- jQuery event handling, keyboard support
- jQuery and CSS3 Animation
- Web form creation (with field validation and pagination)
- AJAX operations
- Data persistence, basic CRUD operations implemented in PHP

#### Development Tools
- [Stylizer](http://www.skybound.ca/)
- [Text Wrangler](http://www.barebones.com/products/textwrangler/)
- [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/)

#### Libraries and Frameworks
- [Compass](http://compass-style.org/)
- [animate.css](http://daneden.github.io/animate.css/)
- [jQuery](http://jquery.com/)
- [Regula](https://github.com/vivin/regula)
- [TinySort](http://tinysort.sjeiti.com/)

#### Languages
- HTML5
- CSS3
- Sass
- JavaScript
- PHP

#### Fonts
- HeavyDataRegular
- BigNoodleTitling
- Kenyan Coffee
- Crystal Deco
- Impact
- Consolas