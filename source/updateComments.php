<?php
error_reporting(E_ALL);
header('Content-Type: application/json');

// This script saves the POST input to an XML data file, then returns a JSON response

// Form data (validated in JS file, so assume good input)
$commentAuthor = !empty($_POST['author']) ? preg_replace( '/[^\.\-\' a-zA-Z0-9]/', '', $_POST['author'] ) : 'Anonymous';
$commentText   = $_POST['text'];
$xmlFormat     = $_POST['xmlFormat'];
$dbFilePath    = $_POST['dbFile'];

// Misc value inits
$t = time() - (60 * 60);
$date = date('F',$t) . ' ' . date('j',$t) . ', ' . date('g',$t) . ':' . date('i',$t) . '  ' . date('A',$t);

// Server response as JSON object containing message, error status, and an array of additional data
function response($msg, $error=false, $errorData='') {
	switch ($msg) {
		case 'success'      : $msgText = 'data successfully written'; break;
		// Errors
		case 'improperXML'  : $msgText = 'xml file is improperly formatted'; break;
		case 'badXML'       : $msgText = 'xml file is corrupt' . PHP_EOL . 'Expected XML root element: "' . $errorData[0] . '", was "' . $errorData[1] . '".'; break;
		case 'fileLoadFail' : $msgText = 'failed to access database'; break;
		case 'fileSaveFail' : $msgText = 'failed to write to database'; break;
		// Accept custom message string
		default             : $msgText = $msg;
	}
	// Return error data dump
	return json_encode(array('msg' => $msgText, 'error' => $error, 'errorData' => $errorData));
}

// Function to create a new XML node using DOMDocument from given content
// parent node param must be a valid node in the XML tree
function insertXMLNode($domDocument, $contextNode, $name, $attributes=array(), $textContent='') {
	if ($domDocument) {
		if (get_class($domDocument) !== 'DOMDocument') {
			throw new Exception('Error: invalid argument "$domDocument"');
		}
		if (!(get_class($contextNode) === 'DOMElement' ||
			  get_class($contextNode) === 'DOMNode')) {
			throw new Exception('Error: invalid argument "$contextNode"');
		}
		$node = $domDocument->createElement($name);
		$contextNode->appendChild($node);
		foreach ($attributes as $attrName => $attrVal) {
			$attrNode = $domDocument->createAttribute($attrName);
			$attrNode->value = $attrVal;
			$node->appendChild($attrNode);
		}
		if ($textContent !== '') {
			$nodeContent = $domDocument->createTextNode($textContent);
			$node->appendChild($nodeContent);
		}
		return $node; // return a reference to newly created node
	} else {
		throw new Exception('Error: DOMDocument does not exist');
	}
}

$xml = new DOMDocument();
if ($xml->load($dbFilePath)) {
	// Read in comment count ID
	$rootNode = $xml->getElementsByTagName('commentList')->item(0);
	$commentCount = $rootNode->getAttribute('totalComments');

	// Write comment data & server response
	$newComment = insertXMLNode($xml, $rootNode, 'comment', array(
		'id'        => $commentCount,
		'author'    => $commentAuthor,
		'rank'      => 0,
		'upvotes'   => 0,
		'downvotes' => 0,
		'date'      => $date
	), $commentText);
	
	$retData = array();
	array_push($retData, $commentCount);
	array_push($retData, $commentAuthor);
	array_push($retData, $date);
	
	$commentCount += 1;
	
	// Update comment count ID
	$xml->documentElement->setAttribute('totalComments',$commentCount);
	
	// Beautify XML output
	$xmlStr = $xml->saveXML();
	$prettyXML = xmlpp($xmlStr);
	$xml->loadXML($prettyXML);
	
	// Save db File
	if (!$xml->save($dbFilePath)) { exit(response('fileSaveFail',true)); }
	echo(response('success', false, $retData));
	
} else {
	exit(response('fileLoadFail', true));
}

// Snippet below taken from: 'http://gdatatips.blogspot.com/2008/11/xml-php-pretty-printer.html'

/** Prettifies an XML string into a human-readable and indented work of art
 *  @param string $xml The XML as a string
 *  @param boolean $html_output True if the output should be escaped (for use in HTML)
 */
function xmlpp($xml, $html_output = false)
{
    $xml_obj = new SimpleXMLElement($xml);
    $level = 4;
    $indent = 0; // current indentation level
    $pretty = array();

    // get an array containing each XML element
    $xml = explode("\n", preg_replace('/>\s*</', ">\n<", $xml_obj->asXML()));

    // shift off opening XML tag if present
    if (count($xml) && preg_match('/^<\?\s*xml/', $xml[0])) {
        $pretty[] = array_shift($xml);
    }

    foreach ($xml as $el) {
        if (preg_match('/^<([\w])+[^>\/]*>$/U', $el)) {
            // opening tag, increase indent
            $pretty[] = str_repeat(' ', $indent) . $el;
            $indent += $level;
        } else {
            if (preg_match('/^<\/.+>$/', $el)) {
                $indent -= $level;  // closing tag, decrease indent
            }
            if ($indent < 0) {
                $indent += $level;
            }
            $pretty[] = str_repeat(' ', $indent) . $el;
        }
    }
    $xml = implode("\n", $pretty);
    return ($html_output) ? htmlentities($xml) : $xml;
}
