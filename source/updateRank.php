<?php
error_reporting(E_ALL);
header('Content-Type: application/json');

// This script saves the POST input to an XML data file, then returns a JSON response

// POST data (validated in JS file, so assume good input)
$commentID       = $_POST['cid'];
$rankAlteration  = $_POST['rank'];
$dbFilePath      = $_POST['dbFile'];

// Server response as JSON object containing message, error status, and an array of additional data
function response($msg, $error=false, $errorData='') {
	switch ($msg) {
		case 'success'      : $msgText = 'data successfully written'; break;
		// Errors
		case 'improperXML'  : $msgText = 'xml file is improperly formatted'; break;
		case 'badXML'       : $msgText = 'xml file is corrupt' . PHP_EOL . 'Expected XML root element: "' . $errorData[0] . '", was "' . $errorData[1] . '".'; break;
		case 'fileLoadFail' : $msgText = 'failed to access database'; break;
		case 'fileSaveFail' : $msgText = 'failed to write to database'; break;
		// Accept custom message string
		default             : $msgText = $msg;
	}
	// Return error data dump
	return json_encode(array('msg' => $msgText, 'error' => $error, 'errorData' => $errorData));
}

$xml = new DOMDocument();
if ($xml->load($dbFilePath)) {
	// Find correct comment
	foreach ($xml->getElementsByTagName('comment') as $comment) {
		if ($comment->getAttribute('id') === $commentID) { // COMPARE ID
			if ($rankAlteration === 'up') {
				$newRank = $comment->getAttribute('rank') + 1;
				$newUpvotes = $comment->getAttribute('upvotes') + 1;
			} else {
				$newRank = $comment->getAttribute('rank') - 1;
				$newDownvotes = $comment->getAttribute('downvotes') + 1;
			}
			$comment->setAttribute('rank',$newRank);
			$comment->setAttribute('upvotes',$newUpvotes);
			$comment->setAttribute('upvotes',$newDownvotes);
			break;
		}
	}
	
	// Save db File
	if (!$xml->save($dbFilePath)) { exit(response('fileSaveFail',true)); }
	echo(response('success', false));
} else {
	exit(response('fileLoadFail', true));
}